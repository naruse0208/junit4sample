package jp.co.junit4.sample;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class FizzBuzzTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckFizzBuzz_01() {
		System.out.println("checkFizzBuzz_01");
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	@Test
	public void testCheckFizzBuzz_02() {
		System.out.println("checkFizzBuzz_02");
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	@Test
	public void testCheckFizzBuzz_03() {
		System.out.println("checkFizzBuzz_03");
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	@Test
	public void testCheckFizzBuzz_04() {
		System.out.println("checkFizzBuzz_04");
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	@Test
	public void testCheckFizzBuzz_05() {
		System.out.println("checkFizzBuzz_05");
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}
	
	
}
